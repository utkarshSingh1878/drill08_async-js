/*
Problem 2:
    Using callbacks and the fs module's asynchronous functions, do the following:
*/
const fs = require("fs");
const path = require("path");

const dataDirPath = path.join(__dirname, "./data/")

const newUpperCaseFileName = 'upperCase.txt'
const newNamesFileName = 'filenames.txt'
const splitDataFileName = 'split.txt'
const splitSortedDataFileName = 'splitAndSortedData.txt'


function taskOneandTwo() {
    try {
        // TODO: 1. Read the given file lipsum.txt
        fs.readFile(`${dataDirPath}/lipsum.txt`, (err, data) => {
            if (err) { throw err }
            // else{console.log(data.toString())}
            else {

                //TODO:!2. Convert the content to uppercase & write to a new file.Store the name of the new file in filenames.txt
                const uppercaseData = data.toString().toUpperCase()
                fs.writeFile(`${dataDirPath}/${newUpperCaseFileName}`, uppercaseData, (err) => {
                    if (err) { throw err }
                    else { console.log(`uppercase file created`) }
                })
            }
        })
    }
    catch (err) {
        console.error(err)
    }
}

function taskThree() {
    try {
        // TODO: 3. Read the new file and convert it to lower case. 
        fs.readFile(`${dataDirPath}/${newUpperCaseFileName}`, (err, data) => {
            const lowerCaseData = data.toString().toLowerCase()
            // console.log(lowerCaseData)
            //! Then split the contents into sentences.Then write it to a new file.
            const splitData = [...lowerCaseData.split(".")]
            // console.log(splitData)
            splitData.forEach((data) => {
                fs.writeFile(`${dataDirPath}/${splitDataFileName}`, data, { flag: "a" }, (err) => {
                    if (err) { throw err }
                })
            })
            console.log(`split data file created successfully`)
        })
        //TODO: Store the name of the new file in filenames.txt
        fs.writeFile(`${dataDirPath}/${newNamesFileName}`, splitDataFileName, (err) => {
            if (err) throw err
            else { console.log('filenames.txt created sucessfully') }
        })
    }
    catch (err) {
        console.error(err)
    }

}

function taskFour() {
    try {
        // TODO: 4. Read the new files, sort the content, write it out to a new file.
        fs.readFile(`${dataDirPath}/${splitDataFileName}`, (err, data) => {
            // console.log(data.toString().split(',').sort());
            const spliSortedData = data.toString().split(',').sort()
            fs.writeFile(`${dataDirPath}/${splitSortedDataFileName}`, spliSortedData.toString(), (err) => {
                if (err) throw err
                else { console.log('splitAndSorted file is created') }
            })
        })
        //TODO: Store the name of the new file in filenames.txt
        fs.writeFile(`${dataDirPath}/${newNamesFileName}`, `\n ${splitSortedDataFileName}`, { flag: "a" }, (err) => {
            if (err) { throw err; }
            else { console.log('splitSortedDataFileName added to the filenames.txt') }
        })
    }
    catch (err) {
        console.error(err)
    }
}

function taskFive() {
    try {
        //TODO: 5. Read the contents of filenames.txt 
        fs.readFile(`${dataDirPath}/${newNamesFileName}`, (err, data) => {
            if (err) throw err
            else {
                //TODO: and delete all the new files that are mentioned in that list simultaneously.
                const filenames = data.toString().split('\n')
                filenames.forEach(filename => {
                    fs.unlink(`${dataDirPath}/${filename}`, (err) => {
                        if (err) throw err
                        else { console.log(`deleted the file ${filename} `) }
                    })
                })
            }
        })
    }
    catch (err) {
        console.error(err)
    }
}
const resTaskOneandTwo = taskOneandTwo()
// console.log(resTaskOneandTwo)
if (resTaskOneandTwo) {
    const resTaskThree = taskThree()
    // console.log(resTaskThree)
    if (resTaskThree) {
        const resTaskFour = taskFour()
        // console.log(resTaskFour)
        if (taskFour) {
            const resTaskFive = taskFive()
            // console.log(resTaskFive)
        }
    }
}




