/* 
    TODO:
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const boards = require('./data/boards.json');
const callback2 = require('./callback2');
const callback3 = require('./callback3');

const task5 = () => {
    let info = new Promise((resolve, reject) => {
        setTimeout(async () => {
            //!getting obj for name Thanos in boards.json
            let boardObj = boards.find(obj => obj.name === 'Thanos');
            //getting id for Thanos
            let boardID = boardObj.id;
            //!calling function from callback to get list info for Thanos's id
            let listInfo = await callback2(boardID);
            let listobj = listInfo.filter(obj => obj.name === 'Mind' || obj.name === 'Space');

            //getting id for name mind and space 
            let mindAndSpaceId = listobj.map(obj => obj.id);

            //calling callback function to get cards for mindID and space id
            let cardsInfo = mindAndSpaceId.map(async (ids) => {
                let cardsData = await callback3(ids);
                return cardsData;
            });
            
            const promise4all = Promise.all(cardsInfo);

            resolve(promise4all);

        }, 3000);
    });
    return info;
}
module.exports = task5;
